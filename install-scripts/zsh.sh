#!/bin/bash
set -e

USER_NAME=$(whoami)

echo "OH MY ZSH INSTAL AND CONFIG FOR $USER_NAME"

# install oh-my-zsh and copy config
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

cat > ~/.zshrc <<\EOF 
    export ZSH="${HOME}/.oh-my-zsh"
    export PATH="$PATH:${HOME}/.local/bin"
    ZSH_THEME="agnoster"
    plugins=(git)
    source $ZSH/oh-my-zsh.sh
EOF

## change user default shell

sudo chsh -s /bin/zsh $USER_NAME