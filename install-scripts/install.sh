#!/bin/bash
set -e

USER_NAME=$1

echo "RUN SCRIPTS FILES"

for file in $(dirname $0)/*.sh 
do
    if [ $file != $0 ]
    then
        echo $file
        chmod +x $file
        su $USER_NAME bash -c $file
    fi
done