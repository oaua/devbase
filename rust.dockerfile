FROM oaua/devbase:base

# Install build dependencies
RUN sudo apt-get -y install build-essential

# Install rust
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s - -y

# Specific install
RUN $HOME/.cargo/bin/cargo install cargo-watch