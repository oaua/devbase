FROM bitnami/python:latest

######## GENERAL INSTALL ########

# CONFIGS

ARG USER_NAME=dev
ARG USER_ID=1000
ARG GROUP_NAME=$USER_NAME
ARG GROUP_ID=$USER_ID
ARG SCRIPT_DIR=/tmp/install-scripts

# Update
RUN apt-get update && apt-get upgrade -y

# User creation
RUN groupadd -g $GROUP_ID $GROUP_NAME -r \
&& useradd -r -g $GROUP_NAME -u $USER_ID $USER_NAME -m

# User sudo usage
RUN apt-get install -y sudo \
    && echo $USER_NAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USER_NAME \
    && chmod 0440 /etc/sudoers.d/$USER_NAME

# Dev tools installation with APK
RUN apt-get install -y \
    curl \
    openssh-client \
    git \
    zsh \
    fonts-powerline \
    locales-all 

# Dev tools installation with Scripts
COPY install-scripts $SCRIPT_DIR
RUN bash $SCRIPT_DIR/install.sh $USER_NAME
RUN rm -r $SCRIPT_DIR

# Specific install
RUN pip install autopep8
RUN chown $USER_NAME:$GROUP_NAME /opt/bitnami/python -R

# Base directory config
ARG APP_PATH=/app
RUN sudo mkdir -p $APP_PATH && chown $USER_NAME:$GROUP_NAME $APP_PATH
WORKDIR $APP_PATH
USER $USER_NAME

CMD ["/bin/zsh"]